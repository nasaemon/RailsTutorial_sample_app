# frozen_string_literal:true

class User < ApplicationRecord
  attr_accessor :remember_token, :activation_token, :reset_token

  before_save :downcase_email
  before_create :create_activation_digest

  has_secure_password
  has_many :microposts, dependent: :destroy
  has_many :active_relationships, class_name: 'Relationship', foreign_key: 'follower_id', dependent: :destroy
  has_many :passive_relationships, class_name: 'Relationship', foreign_key: 'followed_id', dependent: :destroy
  has_many :followings, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true,
                    length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false } # Ignore whether it's upper case or lower case
  validates :name, presence: true, length: { maximum: 50 }
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  # Rerurn hash value of the given string
  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Return random token
  def self.new_token
    SecureRandom.urlsafe_base64
  end

  # Return status feed
  def feed
    followings_ids = 'SELECT followed_id from relationships WHERE follower_id = :user_id'
    Micropost.where("user_id IN (#{followings_ids}) OR user_id = :user_id", user_id: id)
  end

  # Follow the given user
  def follow(other_user)
    followings << other_user
  end

  # Unfollow the given user
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id)&.destroy
  end

  # Return true if following the given user
  def following?(other_user)
    active_relationships.where(followed_id: other_user.id).any?
  end

  # Save remember_digest to DB for eternally continuing sessio
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Return true if the given token corresponds to the digest
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?

    BCrypt::Password.new(digest).is_password?(token)
  end

  # Activate user
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end

  # Send the email for account activation
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Create token for reset password
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now)
  end

  # Send email for reset password
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Return true if the reset_token is expired
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Discard the user's login information
  def forget
    update_attribute(:remember_digest, nil)
  end

  private

  def downcase_email
    self.email = email.downcase
  end

  # Create activation_token and insert it into activation_digest
  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
end

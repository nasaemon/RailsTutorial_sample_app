# frozen_string_literal: true

class Micropost < ApplicationRecord
  belongs_to :user

  default_scope -> { order(created_at: :desc) }

  mount_uploader :picture, PictureUploader

  validates :content, presence: true, length: { maximum: 140 }
  validates :user_id, presence: true
  validate :picture_size

  private

  # Validate the file size of uploaded picture
  def picture_size
    errors.add(:picture, 'File size should be less than 5MB') if picture.size > 5.megabyte
  end
end

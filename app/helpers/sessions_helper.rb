# frozen_string_literal: true

module SessionsHelper
  # Login with the given user
  def log_in(user)
    session[:user_id] = user.id
  end

  def logged_in?
    !current_user.nil?
  end

  # Make given user's session eternal
  def remember(user)
    user.remember # Save user's remember_digest to DB
    cookies.permanent.signed[:user_id] = user.id # Save encrypted user id to cookies
    cookies.permanent[:remember_token] = user.remember_token # remember_token is already hashed
  end

  # Return currently logged in user
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user&.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # Return true if the given user is current user
  def current_user?(user)
    user == current_user
  end

  # Discard eternal session
  def forget(user)
    user.forget # remember_digest -> nil
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # Logout the current user
  def log_out
    forget(current_user) # user_id & remember_token are removed from cookies
    session.delete(:user_id)
    @current_user = nil
  end

  # Redirect to the URL stored in the session or to the default URL
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Remember the URL which user tries to access in session
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end

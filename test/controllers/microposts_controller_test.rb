# frozen_string_literal: true

require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @micropost = microposts(:orange) # Michael's micropost
  end

  test 'should redirect create when not logged in' do
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: 'Lorem ipsum' } }
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy when not logged in' do
    assert_no_difference 'Micropost.count' do
      delete micropost_path(@micropost)
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy for wrong micropost' do
    log_in_as(@user)
    micropost = microposts(:ants) # archer's micropost
    assert_no_difference 'Micropost.count' do
      delete micropost_path(micropost)
    end
    assert_redirected_to root_url
  end

  test 'Should render home when failed to destroy' do
    log_in_as(@user)
    Micropost.stub_any_instance(:destroy, false) do
      assert_no_difference 'Micropost.count' do
        delete micropost_path(@micropost)
      end
      assert_select 'div.alert.alert-danger'
      assert_template 'static_pages/home'
    end
  end
end

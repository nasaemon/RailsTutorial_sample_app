# frozen_string_literal: true

require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    ActionMailer::Base.deliveries.clear
  end

  test 'password reset' do
    # Test for new and create action
    get new_password_reset_path
    assert_template 'password_resets/new'

    # Invalid email
    post password_resets_path, params: { password_reset: { email: '' } }
    assert_not flash.empty?
    assert_template 'password_resets/new'

    # Valid email
    post password_resets_path, params: { password_reset: { email: @user.email } }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_url

    # Test for edit and update action
    user = assigns(:user)

    # Valid token and wrong email
    get edit_password_reset_path(user.reset_token, email: '')
    assert_redirected_to root_url

    # Non-activated user
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url

    # Followig tests are for activated user
    user.toggle!(:activated)

    # Invalid token and correct email
    get edit_password_reset_path('Invalid token', email: user.email)
    assert_redirected_to root_url

    # Valid token and correct email
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select 'input[name=email][type=hidden][value=?]', user.email

    # Check submitted password in the following three patch requests
    # Invalid password and confirmation pair
    patch password_reset_path(user.reset_token), params: { email: user.email,
                                                           user: { password: 'foobaz',
                                                                   password_confirmation: 'barquux' } }
    assert_template 'password_resets/edit'

    # Empty password and confirmation
    patch password_reset_path(user.reset_token), params: { email: user.email,
                                                           user: { password: '',
                                                                   password_confirmation: '' } }
    assert_template 'password_resets/edit'

    # Valid password and confirmation
    patch password_reset_path(user.reset_token), params: { email: user.email,
                                                           user: { password: 'foobaz',
                                                                   password_confirmation: 'foobaz' } }
    assert is_logged_in?
    assert_not flash.empty?
    assert_redirected_to user
  end

  test 'expired reset_token' do
    get new_password_reset_path
    post password_resets_path, params: { password_reset: { email: @user.email } }

    @user = assigns(:user)
    @user.update_attribute(:reset_sent_at, 3.hours.ago)
    patch password_reset_path(@user.reset_token), params: { email: @user.email,
                                                            user: { password: 'foobaz',
                                                                    password_confirmation: 'foobaz' } }
    assert_response :redirect
    follow_redirect!
    assert_match(/expired/i, response.body)
  end
end

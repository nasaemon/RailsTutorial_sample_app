# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/stub_any_instance'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper
  # Add more helper methods to be used by all tests here...

  # Return true if test user is logged in
  def is_logged_in?
    !session[:user_id].nil?
  end

  # Login as a test user
  def log_in_as(user)
    session[:user_id] = user.id
  end
end

class ActionDispatch:: IntegrationTest
  # Login as a test user
  def log_in_as(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me } }
  end
end

# frozen_string_literal: true

# Add unique index to email for uniqueness validation
class AddIndexToUsersEmail < ActiveRecord::Migration[5.2]
  def change
    add_index :users, :email, unique: true
  end
end

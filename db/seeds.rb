# frozen_string_literal: true

# User
User.create!(name: 'Example User',
             email: 'example@railstutorial.org',
             password: 'foobar',
             password_confirmation: 'foobar',
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

# Fake users
99.times do |n|
  name = Faker::Name.name
  email = "example-#{n}@railstutorial.org"
  password = 'password'
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

# Micropost
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# Relationship
users = User.all
first_user = users.first
followings = users[2..50]
followers = users[3..40]
followings.each { |followed| first_user.follow(followed) }
followers.each { |follower| follower.follow(first_user) }

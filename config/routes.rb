# frozen_string_literal: true

Rails.application.routes.draw do
  # Routes for static_pages controller
  root 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'

  # Routes for users controller
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  resources :users do
    member do
      get :followings, :followers
    end
  end

  # Routes for sessions controller
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  # Routes for account_activations controller
  resources :account_activations, only: [:edit]

  # Routes for password reset controller
  resources :password_resets, only: %i[new create edit update]

  # Routes for microposts controller
  resources :microposts, only: %i[create destroy]

  # Routes for relationship controller
  resources :relationships, only: %i[create destroy]
end
